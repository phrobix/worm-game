// Game objects
var Game = {

	"info" 		 : "Game to browse trough WORM's archive",
	"debug"		 : true,
	"root" 		 : this,
	"keysEnabled": false,
	"clickEnabled": false,
	"started"	 : false,

	"library" 	 : null,
	"WORM" 		 : null,

	"init" 		 : function()
	{
		if(Game.debug == true)
			console.log("Game initialized");

		var methods = {
			"setWorm" 	: function(worm) 	{ Game.WORM = worm },
			"setLib" 	: function(lib)	{ Game.library = lib },
			"start" 	: function() {
				if(Game.library != null && Game.WORM != null)
				{
					// Create items from the dataURL
					Game.library.createItems("data/WORM_Data.json", Game.library.shuffle, Game.library.LoadChoices);

					// Enable key checking BOOLEAN
					Game.keysEnabled = true;

					// Enable click checking BOOLEAN
					Game.clickEnabled = true;

					// Setting the started BOOLEAN to true
					Game.started = true;
				}
				else
					if(Game.debug == true)
						console.log("Game cannot start without WORM and/or library");
			},
			"end"		: function()
			{
				$('#itemcontainer').empty();
				var sessionProfile = Game.WORM.WormSessionProfile;

				for(item in sessionProfile)
				{
					if(Game.debug == true)
						console.log(sessionProfile[item]);
				}

				Game.WORM.WormSessionProfile = [];
				Game.keysEnabled = false;
				Game.clickEnabled = false;
				Game.started = false;
			}
		}

		return methods;
	},
}

if(Game.debug == true)
	console.log("Loaded: Game");
