var WORM = function(library)
{
	// Globalizing
	var root = this;

	this.WormSessionProfile = [];

	// Attach this worm to the library
	library.attach(this);

	// Observing functions
	this.actions = {
		'refresh' : function()
		{
			if(Game.debug == true)
				console.log('Library has been refreshed');
		}
	}

	// WORM Methods
	this.update = function(library) { root.actions[library.State](); }
}

if(Game.debug == true)
	console.log("Loaded: WORM");