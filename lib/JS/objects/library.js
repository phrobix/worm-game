var Library = function ()
{
	// Object variables
	this.WORMS = new Array();
	this.State = null;
	this.Items = [];
	this.Results = [];

	this.rightItem, this.leftItem;
	var counter = 0;

	var root = this;

	// Library methods
	this.attach = function(worm) { root.WORMS.push(worm); }
	this.setState = function(state) { root.State = state; root.notify(); }

	this.shuffle = function()
	{
		root.rightItem = 'random right item';
		root.leftItem = 'random left item';

		for(item in root.Items){
			console.log(root.Items[item]);
		}
		if(Game.debug == true)
			console.log(root.Items);

		root.setState('refresh');
	}

	this.notify = function()
	{
		if(Game.debug == true)
			console.log('Library changed');

		for(w in root.WORMS)
		{
			root.WORMS[w].update(root);
		}
	}

	this.createItems = function(dataURL, callback, secondCallback) {
		if(Game.debug == true)
			console.log('Creating items...');

		$.getJSON( dataURL, function(data) {
			if(Game.debug == true)
				console.log('Got JSON data from URL');

			$.each(data, function(index, item){
				var artists = item.artists;

				$.each(artists, function(index, artistFromObject){
					console.log(artistFromObject);
					var artistId = index;
					var artistName = artistFromObject.artist;
					var tags = new Array;
					var image;

					var tagsArray = artistFromObject.tags;
					var imageArray = artistFromObject.image;

					console.log("Artistname: "+artistName);

					if (tagsArray != null || tagsArray != undefined){
						$.each(tagsArray, function(i, tagData){
							if (tagData != null || tagData != undefined){		
								//console.log("tagData: "+tagData)
								var tagName = tagData.name;
								var tagUrl = tagData.url;
								//console.log("tag from "+artistName+": "+tagName);
								tags.push(tagName);
							}
						});
					}
					if (imageArray != null || imageArray != undefined){
						$.each(imageArray, function(i, imageData){
							if (imageData != null || imageData != undefined){		
								//console.log("tagData: "+tagData)
								var imageSize = imageData.size;
								var imageUrl = imageData.text;
								//console.log("image from "+artistName+": "+imageUrl);
								image = imageUrl;
							}
						});
					}
					if(tags.length > 1 || image != ""){
						root.Items.push({
							"id": artistId,
							"artist": artistName,
							"tags" : tags,
							"imgurl" : image
						});
					}
				});
			});
			callback();
			secondCallback();
		});
	}

	this.LoadChoices = function(){

		if (root.Items.length > 1 && counter < 5) {
			counter ++;
			
			var uniqueRandoms = [];
			var numRandoms = root.Items.length;
			var randomItem1;
			var randomitem2;

			function makeUniqueRandom() {
			    // refill the array if needed
			    if (!uniqueRandoms.length) {
			        for (var i = 0; i < numRandoms; i++) {
			            uniqueRandoms.push(i);
			        }
			    }
			    var index = Math.floor(Math.random() * uniqueRandoms.length);
			    var val = uniqueRandoms[index];

			    // now remove that value from the array
			    uniqueRandoms.splice(index, 1);
			    return val;
			}

			var randomItem1 = root.Items[makeUniqueRandom()];
			var randomItem2 = root.Items[makeUniqueRandom()];
			
			while (randomItem1.id == randomItem2.id) {
				randomItem2 = root.Items[makeUniqueRandom()];
				console.log('create new random item!');
				if(randomItem1 != randomItem2){
					break;
				}
			}

			$('#item1').fadeOut(200, function(){
				$('#item1').empty();

				console.log("randomitem1: "+randomItem1.id);
				$('#item1').append('<p class="hiddenid">'+randomItem1.id+'</p><img class="artist-image" src="'+randomItem1.imgurl+'" alt=""><div class="tags"><ul></ul></div>');
				for(tag in randomItem1.tags){
					if ($('#item1 li').length <2) {
						$('#item1 ul').append('<li>'+randomItem1.tags[tag]+'</li>');
					}else{
						break;
					}
				}
				$('#item1').fadeIn(200);
			});

			$('#item2').fadeOut(200, function(){
				$('#item2').empty();

				console.log("randomitem2: "+randomItem2.id);
				$('#item2').append('<p class="hiddenid">'+randomItem2.id+'</p><img class="artist-image" src="'+randomItem2.imgurl+'" alt=""><div class="tags"><ul></ul></div>');
				for(tag in randomItem2.tags){
					if ($('#item2 li').length <2) {
						$('#item2 ul').append('<li>'+randomItem2.tags[tag]+'</li>');
					}else{
						break;
					}
				}
				$('#item2').fadeIn(200);

			});	
		}else{
			console.log('end the game!');

			root.SortResults();
			$('#game-container').fadeOut(200, function(){
				$('#mainscreen').hide();
				$('#mainscreen').load('template/WORMtemplate/flyest.html', function(){
					console.log('LOADED flyest.html');
					
					var numberOfResults = 0;

					$.each(root.Results, function(index, result){					
						numberOfResults ++;
						var tagName = result[0];
						var amountOfTags = result[1];
						$('#content-result .tags').append('<li>'+tagName+' x '+amountOfTags+'</li>');
						if(numberOfResults == 5){
							return false; // break each when 5 results are given
						}
					});

					//load end screen
					$('#mainscreen').fadeIn(1000);
				});
			});
		}
	
	}

	this.SelectChoice = function(target){
		event.preventDefault();
		
		var choiceId = $(target).find('.hiddenid').html();
		var otherChoiceId;

		var tagArray = new Array();

		for(item in root.Items){
			if(root.Items[item].id == choiceId){
				tagArray = root.Items[item].tags;
				root.Items.splice(item, 1);
			}
		}

		for(item in root.Items){
			console.log(root.Items[item]);
		}


		$.each(root.WORMS, function(index, worm){
			for(tag in tagArray){
				worm.WormSessionProfile.push(tagArray[tag]);
			}
		});

		console.log(root.WORMS);
		tagArray.splice(0,tagArray.length);
	}

	this.SortResults = function(){
		var allTags = new Array();

		$.each(root.WORMS, function(index, worm){
			for(tag in worm.WormSessionProfile){
				allTags.push(worm.WormSessionProfile[tag]);
			}
		});

		console.log(allTags);

		var items = {}, sortableItems = [], i, len, element,
		    listOfStrings = allTags;

		for (i = 0, len = listOfStrings.length; i < len; i += 1) {
		    if (items.hasOwnProperty(listOfStrings[i])) {
		        items[listOfStrings[i]] += 1;
		    } else {
		        items[listOfStrings[i]] = 1;
		    }
		}

		for (element in items) {
		    if (items.hasOwnProperty(element)) {
		        root.Results.push([element, items[element]]);
		    }
		}

		root.Results.sort(function (first, second) {
		    return second[1] - first[1];
		});

		console.log(root.Results);
	}
}


if(Game.debug == true)
	console.log("Loaded: Library");
