// Build keydata
var key = { 'left' : 37, 'right' : 39, 'space' : 32 }

// Start script
$(document).ready(function()
{
	$('body').fadeIn(1500, function(){
		$('#continue').fadeIn(1500);
	});
	

	// Initialize Game
	var iGame = Game.init();

	// Build library
	var library = new Library();

	// Draw worm
	var worm = new WORM(library);

	// Add WORM and Library to initialized game
	iGame.setWorm(worm);
	iGame.setLib(library);

	// Watch keys if enabled
	$(this).keydown(function(event)
	{
		// Start game if haven't started
		if(event.which == key.space)
		{
			if(Game.started == false)
			{
				iGame.start();

				if(Game.debug == true)
					console.log('Start game');
			}
			else
			{
				iGame.end();

				if(Game.debug == true)
					console.log('Ended game');
			}
		}

		if(Game.keysEnabled == true)
		{
			switch(event.which)
			{
				case key.left :

					// Make left choice
					if(Game.debug == true)
						console.log('Choose left');

					break;
				case key.right :

					// Make right choice
					if(Game.debug == true)
						console.log('Choose right');

					break;
			}

		}
	});

	$(this).mousedown(function(event){
		//Click handlers
		$('#continue').on('click', function(){
			event.preventDefault();
			if(Game.started == false)
			{
				$('#splashscreen').fadeOut(400, function(){
					$('#mainscreen').hide();
					$('#mainscreen').load('template/WORMtemplate/instructie.html', function(){
						console.log('LOADED instructie.html');
						$('#mainscreen').fadeIn(400);
					});
				});
			}
		});


		$('#startgame').on('click', function(){
			event.preventDefault();
			if(Game.started == false)
			{
				
				$('#instructieScherm').fadeOut(400, function(){
					$('#mainscreen').load('template/WORMtemplate/keuze2.html', function(){
						console.log('LOADED keuze2.html');
						iGame.start();
						$('#game-container').fadeIn(1000);
					});
				});

				if(Game.debug == true)
					console.log('Start game');
			}
			else
			{
				iGame.end();

				if(Game.debug == true)
					console.log('Ended game');
			}
		});


		$('#resultButton').on('click', function(){
			event.preventDefault();
			console.log('load results...');
			$('#content').fadeOut(500, function(){
				$('#content-result').fadeIn(1000);
			});
		});

		if(event.which == 1){
			if(Game.clickEnabled == true){
				$('#item1').on('click', function(event){
					event.preventDefault();
					if(Game.debug == true)
						console.log('Item 1 clicked');
					
					var target = $(this);
					Game.library.SelectChoice(target);
					Game.library.LoadChoices();
					event.stopImmediatePropagation();
  					return false;
				});
				$('#item2').on('click',function(event){
					event.preventDefault();
					if(Game.debug == true)
						console.log('Item 2 clicked');

					var target = $(this);
					Game.library.SelectChoice(target);
					Game.library.LoadChoices();
					event.stopImmediatePropagation();
  					//return false;
				});
			}
		}

	});


});
