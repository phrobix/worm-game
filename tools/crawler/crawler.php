<?php
ini_set('display_errors', '1');
error_reporting(E_ALL);

// Getting artist info from LAST.FM
function lastFMApiCallUrl($artist) {
	return "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=".urlencode($artist)."&api_key=b3d2be5d88811cb3f117810cb615645f&format=json";
}

$domain = "http://www.worm.org";
$url = $domain."/home/filter/concert";

//Start connection
$con = curl_init();

//Execute some options
curl_setopt($con, CURLOPT_URL, $url);
curl_setopt($con, CURLOPT_FRESH_CONNECT, TRUE);
curl_setopt($con, CURLOPT_COOKIEJAR, 'cookie.txt');
curl_setopt($con, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko');
curl_setopt($con, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($con, CURLOPT_HEADER, TRUE);
curl_setopt($con, CURLOPT_FOLLOWLOCATION, TRUE);

//Fetch the answer
$conContent = curl_exec($con);

curl_close($con);

//echo '<pre>'.nl2br(htmlentities($conContent)).'</pre>';

$dom = new DOMDocument();
@$dom->loadHTML($conContent);

$xpath = new DOMXPath($dom);
$href = $xpath -> query('/html/body//div[@id="appboard"]/div[@id="apps"]/ul/*/div/a');

//echo '<pre>'.print_r($href,true).'</pre>';
$concerts = [];
$id = 0;
$aid = 1;
foreach($href AS $dataContainer)
{
	$delete = false;

	$subUrl = $domain.$dataContainer -> getAttribute('href');

	//Start connection
	$con = curl_init();

	//Execute some options
	curl_setopt($con, CURLOPT_URL, $subUrl);
	curl_setopt($con, CURLOPT_FRESH_CONNECT, TRUE);
	curl_setopt($con, CURLOPT_COOKIEJAR, 'cookie.txt');
	curl_setopt($con, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko');
	curl_setopt($con, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($con, CURLOPT_HEADER, TRUE);
	curl_setopt($con, CURLOPT_FOLLOWLOCATION, TRUE);

	//Fetch the answer
	$conContent = curl_exec($con);

	curl_close($con);

	$dom = new DOMDocument();
	@$dom->loadHTML(utf8_decode($conContent));

	$xpath = new DOMXPath($dom);
	$Titles = $xpath -> query('/html/body//div[@id="appboard"]/div[@id="apps"]/*/*/*/*/div[@id="pageTitle"]/h1');
	$Dates = $xpath -> query('/html/body//div[@id="appboard"]/div[@id="apps"]/*/*/*/*/div[@id="pageDate"]');
	$ConcertInfo = $xpath -> query('/html/body//div[@id="appboard"]/div[@id="apps"]/*/*/*/*/div[@id="pageInfoContainer"]/div/*');
	$ConcertDescription = $xpath -> query('/html/body//div[@id="appboard"]/div[@id="apps"]/*/*/*/div[@id="pageArticle"]');
	
	$monthArray["short"]= ["jan", "feb", "maa", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"];
	$monthArray["long"]	= ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"];
	$dayArray["short"]	= ["ma", "di", "wo", "do", "vr", "za", "zo"];
	$dayArray["long"]	= ["maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"];	

	foreach($Titles as $title) { $concert['title'] = htmlentities(trim($title -> nodeValue)); }
	foreach($Dates as $date) { $concert['date'] = 	str_replace($monthArray['short'], $monthArray['long'], 
													str_replace($dayArray['short'], $dayArray['long'], 
													str_replace("/", " ", trim($date -> nodeValue)))); }
	foreach($ConcertInfo as $cInfo) {
		switch(strtolower(trim($cInfo -> getAttribute("class"))))
		{
			case 'doorsopen' : $concert['doorsopen'] = substr(trim(end(explode("Open: ", $cInfo -> nodeValue))), 0, -1); break;
			case 'starttime' : $concert['starttime'] = substr(trim(end(explode("Start:", $cInfo -> nodeValue))), 0, -1); break;
			case 'endtime' : $concert['endtime'] = substr(trim(end(explode("Einde:", $cInfo -> nodeValue))), 0, -1); break;
		}
	}
	foreach($ConcertDescription as $cDesc) { $concert['description'] = nl2br(htmlentities(trim($cDesc -> nodeValue))); }
	
	foreach(explode(' + ', $concert['title']) as $artist)
	{
		$lastFMJSON = json_decode(file_get_contents(lastFMApiCallUrl($artist)));
		
		if(isset($lastFMJSON -> artist -> tags) && $lastFMJSON -> artist -> tags)
			$tags = $lastFMJSON -> artist -> tags -> tag;
		if(isset($lastFMJSON -> artist -> image) && $lastFMJSON -> artist -> image)
			$image = $lastFMJSON -> artist -> image;

		$concert['artists'][$aid++] = array("artist" => $artist, "tags" => $tags, "image" => $image);
	}
	
	$concert['id'] = $id;
	$id++;

	//if(isset($concert['tags']) && !empty($concert['tags']))
		$concerts[] = $concert;
	/*else
	{
		$concert = null;
		$delete = false;
	}*/
	$concert = null;
}

$concertData = $concerts;
$JSONFile = fopen("API/Data/WORM_Data.json", "w+");
fwrite($JSONFile, json_encode($concertData));
fclose($JSONFile);
?>