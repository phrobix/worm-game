<?php
session_start();
ini_set('display_errors', '1');
error_reporting(E_ALL);

// Admin interface
if(isset($_SESSION['hash']) && !empty($_SESSION['hash']) && $_SESSION['hash'] == "UniekeHashKeyFake")
{
	if(isset($_GET['actie']) && $_GET['actie'] == "toevoegen")
	{
		if(isset($_POST['add']) && $_POST['add'])
		{
			$object[] = Array(	"artist" 	=> $_POST['naam'],
								"album"		=> $_POST['album'],
								"genre"		=> $_POST['genre']);
			$data = json_decode(file_get_contents("data/archief.json"), true);

			if(is_array($data))
				$newData = array_merge($object, $data);
			else
				$newData = $object;

			$JSONFile = fopen("data/archief.json", "w+");
			fwrite($JSONFile, json_encode($newData));
			fclose($JSONFile);

			echo 'Item is succesvol toegevoegd, <a href="admin.php">Klik hier</a> om door te gaan.';
		}
		else
		{
			echo '
			<form method="post" action="">
				<fieldset>
					<input type="text" name="naam" placeholder="artiest" />
					<input type="text" name="album" placeholder="album" />
					<input type="text" name="genre" placeholder="genre" />
					<input type="submit" name="add" value="Toevoegen" />
				</fieldset
			</form>';
		}
	}
	elseif(isset($_GET['actie']) && $_GET['actie'] == "bekijken")
	{
		$data = json_decode(file_get_contents("data/archief.json"));
		$increment = 0;
		echo '<ul>';
		foreach($data as $item)
		{
			echo '<li>'.$item -> artist .' ('. $item -> album .') - <a href="admin.php?actie=verwijderen&id='.$increment.'">D</a> - <a href="admin.php?actie=bewerken&id='.$increment.'">E</a>  </li>';
			$increment++;
		}
		echo '</ul>';
	}
	elseif(isset($_GET['actie']) && $_GET['actie'] == "verwijderen" && isset($_GET['id']))
	{
		$data = json_decode(file_get_contents("data/archief.json"), true);
		$item = $data[$_GET['id']];

		if(isset($_POST['del']) && $_POST['del'])
		{
			unset($data[$_GET['id']]);
			$JSONFile = fopen("data/archief.json", "w+");
			fwrite($JSONFile, json_encode($data));
			fclose($JSONFile);

			echo 'Item is succesvol verwijderd, <a href="admin.php">Klik hier</a> om door te gaan.';
		}
		else
		{
			echo 'Weet u zeker dat u het album: '.$item['album'].' van '.$item['artist'].' wil verwijderen?<br />
			<form method="post" action=""><fieldset><input type="submit" name="del" value="Verwijder" /></fieldset></form>';
		}
	}
	elseif(isset($_GET['actie']) && $_GET['actie'] == "bewerken" && isset($_GET['id']))
	{
		$data = json_decode(file_get_contents("data/archief.json"), true);
		$item = $data[$_GET['id']];

		if(isset($_POST['save']) && $_POST['save'])
		{
			$data[$_GET['id']] = Array(	"artist" 	=> $_POST['naam'],
										"album"		=> $_POST['album'],
										"genre"		=> $_POST['genre']);

			$JSONFile = fopen("data/archief.json", "w+");
			fwrite($JSONFile, json_encode($data));
			fclose($JSONFile);

			echo 'Item is succesvol bewerkt, <a href="admin.php">Klik hier</a> om door te gaan.';
		}
		else
		{
			echo '
			<form method="post" action="">
				<fieldset>
					<input type="text" name="naam" placeholder="artiest" value="'.$item['artist'].'" />
					<input type="text" name="album" placeholder="album" value="'.$item['album'].'"  />
					<input type="text" name="genre" placeholder="genre" value="'.$item['genre'].'"  />
					<input type="submit" name="save" value="opslaan" />
				</fieldset
			</form>';
		}
	}
	else
	{
		echo '<a href="admin.php?actie=toevoegen">Item aan archief toevoegen</a><br />
		  <a href="admin.php?actie=bekijken">Archief bekijken</a>';
	}
}
else
{
	if(isset($_POST['login']) && $_POST['login'])
	{
		if(	isset($_POST['user']) && !empty($_POST['user']) && $_POST['user'] == "worm" &&
		 	isset($_POST['pass']) && !empty($_POST['pass']) && $_POST['pass'] == "W0rm" )
		{
			$_SESSION['hash'] = "UniekeHashKeyFake";
			echo 'U bent succesvol ingelogd, <a href="admin.php">Klik hier</a> om door te gaan';
		}
		else
			echo 'U heeft een verkeerde gebruikersnaam en/of wachtwoord opgegeven.';
	}
	else
	{
		echo 'Om deze pagina te zien moet u inloggen: <br /><br />
		<form method="post" action="">
			<fieldset>
				<input type="text" name="user" placeholder="Gebruikersnaam" />
				<input type="password" name="pass" placeholder="Wachtwoord" />
				<input type="submit" name="login" value="Log in" />
			</fieldset>
		</form>';
	}
}


?>